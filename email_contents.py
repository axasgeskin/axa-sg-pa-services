client_text = """
Certificate No. %(reference)s


Dear Customer,

Your transaction is successful and you are covered for your personal accident.
Thank you for choosing AXA Insurance Singapore as your insurer for your HSBC Protect 150.


Period of Insurance : %(start_date)s - %(end_date)s
Policyholder : Included
Spouse : %(spouse)s
No. of Children : %(children)s
Total Premium : $ %(premium)s

Benefits / Sum insured
Accidental Death / $150,000
Permanent total disablement/$150,000


For any clarification on your policy, please contact our Customer Care Department at 1800 880 4888 (Within Singapore) or 6880 4888 (International).

This is a computer generated letter. No signature is required.

-
AXA INSURANCE SINGAPORE PTE LTD
8 Shenton Way #27-01, AXA Tower Singapore 068811
Customer Care #B1-01
Tel: 1800-880 4741 Fax: 6880 4740
Website: www.axa.com.sg
Co. Reg No. 196900406D

"""




client_text_html = """
<!DOCTYPE html>
<html>
<body>
<img src="cid:id-logo" width="600">
<br>
<br>
<b>Certificate No. :%(reference)s</b><br>
<br>
Dear Customer,<br>
<br>
Your transaction is successful and you are covered for your personal accident. <br>
Thank you for choosing AXA Insurance Singapore as your insurer for your HSBC Protect 150. <br>
<br>
<table>
<tr><td>Period of Insurance :</td><td>%(start_date)s - %(end_date)s</td></tr>
<tr><td>Policyholder :</td><td>Included</td></tr>
<tr><td>Spouse :</td><td>%(spouse)s</td></tr>
<tr><td>No. of Children :</td><td>%(children)s</td></tr>
<tr><td>Total Premium :</td><td>$ %(premium)s</td></tr>
</table>
<br>
<table>
<tr><td><b>Benefits<b></td><td> Sum insured</td></tr>
<tr><td>Accidental Death :</td><td>$150,000</td></tr>
<tr><td>Permanent total disablement: :</td><td>$150,000</td></tr>
</table>

For any clarification on your policy, please contact our Customer Care Department at 1800 880 4888 (Within Singapore) or 6880 4888 (International).<br>
<br>
This is a computer generated letter. No signature is required.<br>
<br>
-<br>
<small>
AXA INSURANCE SINGAPORE PTE LTD<br>
8 Shenton Way #27-01, AXA Tower Singapore 068811<br>
Customer Care #B1-01<br>
Tel: 1800-880 4741 Fax: 6880 4740<br>
Website: www.axa.com.sg<br>
Co. Reg No. 196900406D<br>
</small>
</body>
</html>
"""









client_text2 = """
Certificate No. %(reference)s

Dear Customer,

Thank you for choosing AXA for your personal accident insurance needs. You are now covered and you will receive your policy documents in the coming days.
In the meantime, we remain available via our hotline - 1800 880 4888, or by email customer.service@axa.com.sg.

Total Premium                   :	$ %(premium)s

For emergency assistance, please call our 24-Hour hotline at +65-6322-2566.

This is a computer generated letter. No signature is required.

-
AXA INSURANCE SINGAPORE PTE LTD
8 Shenton Way #27-01, AXA Tower Singapore 068811
Customer Care #B1-01
Tel: 1800-880 4741 Fax: 6880 4740
Website: www.axa.com.sg
Co. Reg No. 196900406D

"""

client_text_html2 = """
<!DOCTYPE html>
<html>
<body>
<img src="cid:id-logo" width="600">
<br>
<br>
<b>Certificate No. :%(reference)s</b><br>
<br>
Dear Customer,<br>
<br>
Thank you for choosing AXA for your personal accident insurance needs. You are now covered and you will receive your policy documents in the coming days.<br>
In the meantime, we remain available via our hotline - 1800 880 4888, or by email customer.service@axa.com.sg.
<table>
<tr><td>Total Premium  :</td><td>$ %(premium)s</td></tr>
</table>
For emergency assistance, please call our 24-Hour hotline at +65-6322-2566.<br>
<br>
This is a computer generated letter. No signature is required.<br>
<br>
-<br>
<small>
AXA INSURANCE SINGAPORE PTE LTD<br>
8 Shenton Way #27-01, AXA Tower Singapore 068811<br>
Customer Care #B1-01<br>
Tel: 1800-880 4741 Fax: 6880 4740<br>
Website: www.axa.com.sg<br>
Co. Reg No. 196900406D<br>
</small>
</body>
</html>
"""