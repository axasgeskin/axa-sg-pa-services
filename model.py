from google.appengine.ext import db

class Wirecard(db.Model):
    transidmerchant = db.StringProperty(required=True)
    totalamount = db.FloatProperty(required=True, default=0.000)
    status = db.StringProperty(required=True, default="INIT")
    paytype = db.StringProperty()
    approvalcode = db.StringProperty()
    bankresponsecode = db.StringProperty()
    trntype = db.StringProperty()
    created = db.DateTimeProperty(auto_now_add=True)
    recurrentid = db.StringProperty()
    data = db.TextProperty()
    error = db.StringProperty()
    errormsg = db.StringProperty()
    mida = db.StringProperty()
    product = db.StringProperty()
    env = db.StringProperty()