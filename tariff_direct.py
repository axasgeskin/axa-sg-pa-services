__author__ = 'clement'
import logging

class Tariff() :

    discount = 0.1
    coeff_manual_occupation = {True : 1.4, False : 1.00}
    coeff_kid = 0.00104
    with_gst = 0.07
    ttd = {0 : 0, 100 : 10.00, 200 : 19.00, 300 : 27.00}
    tpd = {0 : 0, 100 : 15.00, 200 : 29.00, 300 : 40.00}
    ahc = {0 : 0, 100 : 8.00, 200 : 15.00, 300 : 20.00}
    hsi = {'adult' : 45.00, 'kid' : 15.00} #healthcareSeriousInjury
    rssi = {'adult' : 40.00, 'kid' : 12.00} #recoverySupportSerious Injury
    lp = 20.00 #Lifestyle Protection
    dc = 15.00 #Dependedent Cover

    promo_discount = 0.0


    def coeff_si(self,si):
        if( si <= 300000) : coeff =0.00080
        elif(si <= 500000) : coeff= 0.00070
        else : coeff = 0.00060
        return coeff


    def premium(self, si = 0, manual_occupation_main = False ,  spouse=0,  manual_occupation_spouse = False, no_kids = 0, ttd = 0, tpd = 0, ahc = 0, hsi = False, rssi = False, lp = False, dc = False  ) :

        coeff_discount = 1
        if(spouse > 0 or no_kids >0 ) :
            coeff_discount = 1 - self.discount

        logging.info('=== coeff discount===')
        logging.info(coeff_discount)

        p = {}

####################
        p['option_ttd_wo_discount_and_gst_main'] = self.ttd[ttd]* coeff_discount
        p['option_ttd_wo_discount_and_gst_spouse'] =  spouse * self.ttd[ttd]* coeff_discount
        p['option_tpd_wo_discount_and_gst_main'] = self.tpd[tpd]* coeff_discount
        p['option_tpd_wo_discount_and_gst_spouse'] = spouse * self.tpd[tpd]* coeff_discount
        p['option_ahc_wo_discount_and_gst_main'] = self.ahc[ahc]* coeff_discount
        p['option_ahc_wo_discount_and_gst_spouse'] = spouse * self.ahc[ahc]* coeff_discount
        p['option_ahc_wo_discount_and_gst_kid'] = min(no_kids,1) * self.ahc[ahc]* coeff_discount

####################
        p['option_hsi_wo_discount_and_gst_main'] = hsi * self.hsi['adult'] * coeff_discount
        p['option_hsi_wo_discount_and_gst_spouse'] = spouse * hsi * self.hsi['adult'] * coeff_discount
        p['option_hsi_wo_discount_and_gst_kid'] = min(no_kids, 1) * hsi * self.hsi['kid'] * coeff_discount
        p['option_rssi_wo_discount_and_gst_main'] = rssi * self.rssi['adult'] * coeff_discount
        p['option_rssi_wo_discount_and_gst_spouse'] = spouse * rssi * self.rssi['adult'] * coeff_discount
        p['option_rssi_wo_discount_and_gst_kid'] = min(no_kids, 1) * rssi * self.rssi['kid'] * coeff_discount
        p['option_lp_wo_discount_and_gst_main'] = lp * self.lp * coeff_discount
        p['option_lp_wo_discount_and_gst_spouse'] = lp * spouse * self.lp* coeff_discount
        p['option_dc_wo_discount_and_gst_main'] = dc * self.dc * coeff_discount
        p['option_dc_wo_discount_and_gst_spouse'] = dc * spouse * self.dc* coeff_discount


###################
        p['option_total_wo_discount_and_gst_main'] = p['option_ttd_wo_discount_and_gst_main'] + p['option_tpd_wo_discount_and_gst_main'] + p['option_ahc_wo_discount_and_gst_main'] + p['option_hsi_wo_discount_and_gst_main'] + p['option_rssi_wo_discount_and_gst_main'] + p['option_lp_wo_discount_and_gst_main'] + p['option_dc_wo_discount_and_gst_main']
        p['option_total_wo_discount_and_gst_spouse'] = p['option_ttd_wo_discount_and_gst_spouse'] + p['option_tpd_wo_discount_and_gst_spouse'] + p['option_ahc_wo_discount_and_gst_spouse'] + p['option_hsi_wo_discount_and_gst_spouse'] + p['option_rssi_wo_discount_and_gst_spouse'] + p['option_lp_wo_discount_and_gst_spouse'] + p['option_dc_wo_discount_and_gst_spouse']
        p['option_total_wo_discount_and_gst_kid'] = p['option_ahc_wo_discount_and_gst_kid'] + p['option_hsi_wo_discount_and_gst_kid'] + p['option_rssi_wo_discount_and_gst_kid']



####################
        p['premium_wo_discount_and_gst_main'] = si * self.coeff_si(si) * self.coeff_manual_occupation[manual_occupation_main] * coeff_discount + p['option_total_wo_discount_and_gst_main']
        p['premium_wo_discount_main'] = p['premium_wo_discount_and_gst_main'] * (1+self.with_gst)
        p['discount_wo_gst_main'] = p['premium_wo_discount_and_gst_main'] * self.promo_discount
        p['premium_main'] = p['premium_wo_discount_and_gst_main'] - p['discount_wo_gst_main'] + round( (p['premium_wo_discount_and_gst_main'] - p['discount_wo_gst_main']) * self.with_gst, 2)

        p['premium_wo_discount_and_gst_spouse'] = spouse * si * self.coeff_si(si) * self.coeff_manual_occupation[manual_occupation_spouse] * coeff_discount + p['option_total_wo_discount_and_gst_spouse']
        p['premium_wo_discount_spouse'] = p['premium_wo_discount_and_gst_spouse'] * (1+self.with_gst)
        p['discount_wo_gst_spouse'] = p['premium_wo_discount_and_gst_spouse'] * self.promo_discount
        p['premium_spouse'] = p['premium_wo_discount_and_gst_spouse'] - p['discount_wo_gst_spouse'] + round((p['premium_wo_discount_and_gst_spouse']-p['discount_wo_gst_spouse'])*self.with_gst,2)

        p['premium_wo_discount_and_gst_kid'] =  min(no_kids,1) * min(si * 0.5 , 150000)  *  self.coeff_kid  * coeff_discount + p['option_total_wo_discount_and_gst_kid']
        p['premium_wo_discount_kid'] = p['premium_wo_discount_and_gst_kid'] * (1+self.with_gst)
        p['discount_wo_gst_kid'] = p['premium_wo_discount_and_gst_kid'] * self.promo_discount
        p['premium_kid'] = p['premium_wo_discount_and_gst_kid'] - p['discount_wo_gst_kid'] + round((p['premium_wo_discount_and_gst_kid']-p['discount_wo_gst_kid'])*self.with_gst,2)

        p['premium_wo_discount_and_gst_kids'] = p['premium_wo_discount_and_gst_kid'] * no_kids
        p['premium_wo_discount_kids'] = p['premium_wo_discount_and_gst_kids'] * (1+self.with_gst)

        p['discount_wo_gst_kids'] = p['discount_wo_gst_kid'] * no_kids
        p['premium_kids'] = p['premium_kid'] * no_kids

##############
        p['option_ttd_wo_discount_and_gst_main'] = self.ttd[ttd]* coeff_discount
        p['discount_ttd_wo_gst_main'] = p['option_ttd_wo_discount_and_gst_main'] * self.promo_discount
        p['option_ttd_main'] = p['option_ttd_wo_discount_and_gst_main'] - p['discount_ttd_wo_gst_main'] + round((p['option_ttd_wo_discount_and_gst_main'] - p['discount_ttd_wo_gst_main']) * self.with_gst, 2)

        p['option_ttd_wo_discount_and_gst_spouse'] =  spouse * self.ttd[ttd]* coeff_discount
        p['discount_ttd_wo_gst_spouse'] = p['option_ttd_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_ttd_spouse'] = p['option_ttd_wo_discount_and_gst_spouse'] - p['discount_ttd_wo_gst_spouse'] + round((p['option_ttd_wo_discount_and_gst_spouse'] - p['discount_ttd_wo_gst_spouse']) * self.with_gst, 2)

        p['option_ttd'] = p['option_ttd_main'] + p['option_ttd_spouse']
        p['option_ttd_wo_discount'] = (p['option_ttd_wo_discount_and_gst_main'] + p['option_ttd_wo_discount_and_gst_spouse']) * (1+self.with_gst)


#############
        p['option_tpd_wo_discount_and_gst_main'] = self.tpd[tpd]* coeff_discount
        p['discount_tpd_wo_gst_main'] = p['option_tpd_wo_discount_and_gst_main'] * self.promo_discount
        p['option_tpd_main'] = p['option_tpd_wo_discount_and_gst_main'] - p['discount_tpd_wo_gst_main'] + round((p['option_tpd_wo_discount_and_gst_main'] - p['discount_tpd_wo_gst_main']) * self.with_gst, 2)

        p['option_tpd_wo_discount_and_gst_spouse'] = spouse * self.tpd[tpd]* coeff_discount
        p['discount_tpd_wo_gst_spouse'] = p['option_tpd_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_tpd_spouse'] = p['option_tpd_wo_discount_and_gst_spouse'] - p['discount_tpd_wo_gst_spouse'] + round((p['option_tpd_wo_discount_and_gst_spouse'] - p['discount_tpd_wo_gst_spouse']) * self.with_gst, 2)

        p['option_tpd'] = p['option_tpd_spouse'] + p['option_tpd_main']
        p['option_tpd_wo_discount'] = (p['option_tpd_wo_discount_and_gst_main'] + p['option_tpd_wo_discount_and_gst_spouse']) * (1+self.with_gst)


#############
        p['option_ahc_wo_discount_and_gst_main'] = self.ahc[ahc]* coeff_discount
        p['discount_ahc_wo_gst_main'] = p['option_ahc_wo_discount_and_gst_main'] * self.promo_discount
        p['option_ahc_main'] = p['option_ahc_wo_discount_and_gst_main'] - p['discount_ahc_wo_gst_main'] + round((p['option_ahc_wo_discount_and_gst_main'] - p['discount_ahc_wo_gst_main']) * self.with_gst, 2)

        p['option_ahc_wo_discount_and_gst_spouse'] = spouse * self.ahc[ahc]* coeff_discount
        p['discount_ahc_wo_discount_and_gst_spouse'] = p['option_ahc_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_ahc_spouse'] = p['option_ahc_wo_discount_and_gst_spouse'] - p['discount_ahc_wo_discount_and_gst_spouse'] + round((p['option_ahc_wo_discount_and_gst_spouse'] - p['discount_ahc_wo_discount_and_gst_spouse']) * self.with_gst, 2)

        p['option_ahc_wo_discount_and_gst_kid'] = min(no_kids,1) * self.ahc[ahc]* coeff_discount
        p['discount_ahc_wo_gst_kid'] = p['option_ahc_wo_discount_and_gst_kid'] * self.promo_discount
        p['option_ahc_kid'] = p['option_ahc_wo_discount_and_gst_kid'] - p['discount_ahc_wo_gst_kid'] + round((p['option_ahc_wo_discount_and_gst_kid'] - p['discount_ahc_wo_gst_kid']) * self.with_gst, 2)

        p['option_ahc_wo_discount_and_gst_kids'] = no_kids * p['option_ahc_wo_discount_and_gst_kid']
        p['discount_ahc_wo_discount_and_gst_kids'] = no_kids * p['discount_ahc_wo_gst_kid']
        p['option_ahc_kids'] = no_kids * p['option_ahc_kid']

        p['option_ahc'] = p['option_ahc_kids'] + p['option_ahc_spouse'] + p['option_ahc_main']
        p['option_ahc_wo_discount'] = (p['option_ahc_wo_discount_and_gst_main'] + p['option_ahc_wo_discount_and_gst_spouse'] + p['option_ahc_wo_discount_and_gst_kids'])  * (1+self.with_gst)


#############
        p['option_hsi_wo_discount_and_gst_main'] = hsi * self.hsi['adult'] * coeff_discount
        p['discount_hsi_wo_gst_main'] = p['option_hsi_wo_discount_and_gst_main'] * self.promo_discount
        p['option_hsi_main'] =  p['option_hsi_wo_discount_and_gst_main'] - p['discount_hsi_wo_gst_main'] + round((p['option_hsi_wo_discount_and_gst_main'] - p['discount_hsi_wo_gst_main']) * self.with_gst, 2)

        p['option_hsi_wo_discount_and_gst_spouse'] = spouse * hsi * self.hsi['adult'] * coeff_discount
        p['discount_hsi_wo_gst_spouse'] = p['option_hsi_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_hsi_spouse'] =  p['option_hsi_wo_discount_and_gst_spouse'] - p['discount_hsi_wo_gst_spouse'] + round((p['option_hsi_wo_discount_and_gst_spouse'] - p['discount_hsi_wo_gst_spouse']) * self.with_gst, 2)

        p['option_hsi_wo_discount_and_gst_kid'] = min(no_kids, 1) * hsi * self.hsi['kid'] * coeff_discount
        p['discount_hsi_wo_gst_kid'] = p['option_hsi_wo_discount_and_gst_kid'] * self.promo_discount
        p['option_hsi_kid'] =  p['option_hsi_wo_discount_and_gst_kid'] - p['discount_hsi_wo_gst_kid'] + round((p['option_hsi_wo_discount_and_gst_kid'] - p['discount_hsi_wo_gst_kid']) * self.with_gst, 2)

        p['option_hsi_wo_discount_and_gst_kids'] = no_kids * p['option_hsi_wo_discount_and_gst_kid']
        p['discount_hsi_wo_discount_and_gst_kids'] = no_kids * p['discount_hsi_wo_gst_kid']
        p['option_hsi_kids'] = no_kids * p['option_hsi_kid']

        p['option_hsi'] = p['option_hsi_kids'] + p['option_hsi_spouse'] + p['option_hsi_main']
        p['option_hsi_wo_discount'] = (p['option_hsi_wo_discount_and_gst_main'] + p['option_hsi_wo_discount_and_gst_spouse'] + p['option_hsi_wo_discount_and_gst_kids'])  * (1+self.with_gst)


#############
        p['option_rssi_wo_discount_and_gst_main'] = rssi * self.rssi['adult'] * coeff_discount
        p['discount_rssi_wo_gst_main'] = p['option_rssi_wo_discount_and_gst_main'] * self.promo_discount
        p['option_rssi_main'] =  p['option_rssi_wo_discount_and_gst_main'] - p['discount_rssi_wo_gst_main'] + round((p['option_rssi_wo_discount_and_gst_main'] - p['discount_rssi_wo_gst_main']) * self.with_gst, 2)

        p['option_rssi_wo_discount_and_gst_spouse'] = spouse * rssi * self.rssi['adult'] * coeff_discount
        p['discount_rssi_wo_gst_spouse'] = p['option_rssi_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_rssi_spouse'] =  p['option_rssi_wo_discount_and_gst_spouse'] - p['discount_rssi_wo_gst_spouse'] + round((p['option_rssi_wo_discount_and_gst_spouse'] - p['discount_rssi_wo_gst_spouse']) * self.with_gst, 2)

        p['option_rssi_wo_discount_and_gst_kid'] = min(no_kids, 1) * rssi * self.rssi['kid'] * coeff_discount
        p['discount_rssi_wo_gst_kid'] = p['option_rssi_wo_discount_and_gst_kid'] * self.promo_discount
        p['option_rssi_kid'] =  p['option_rssi_wo_discount_and_gst_kid'] - p['discount_rssi_wo_gst_kid'] + round((p['option_rssi_wo_discount_and_gst_kid'] - p['discount_rssi_wo_gst_kid']) * self.with_gst, 2)

        p['option_rssi_wo_discount_and_gst_kids'] = no_kids * p['option_rssi_wo_discount_and_gst_kid']
        p['discount_rssi_wo_discount_and_gst_kids'] = no_kids * p['discount_rssi_wo_gst_kid']
        p['option_rssi_kids'] = no_kids * p['option_rssi_kid']

        p['option_rssi'] = p['option_rssi_kids'] + p['option_rssi_spouse'] + p['option_rssi_main']
        p['option_rssi_wo_discount'] = (p['option_rssi_wo_discount_and_gst_main'] + p['option_rssi_wo_discount_and_gst_spouse'] + p['option_rssi_wo_discount_and_gst_kids'])  * (1+self.with_gst)

#############
        p['option_lp_wo_discount_and_gst_main'] = lp * self.lp * coeff_discount
        p['discount_lp_wo_gst_main'] = p['option_lp_wo_discount_and_gst_main'] * self.promo_discount
        p['option_lp_main'] = p['option_lp_wo_discount_and_gst_main'] - p['discount_lp_wo_gst_main'] + round((p['option_lp_wo_discount_and_gst_main'] - p['discount_lp_wo_gst_main']) * self.with_gst, 2)

        p['option_lp_wo_discount_and_gst_spouse'] = lp * spouse * self.lp* coeff_discount
        p['discount_lp_wo_gst_spouse'] = p['option_lp_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_lp_spouse'] = p['option_lp_wo_discount_and_gst_spouse'] - p['discount_lp_wo_gst_spouse'] + round((p['option_lp_wo_discount_and_gst_spouse'] - p['discount_lp_wo_gst_spouse']) * self.with_gst, 2)

        p['option_lp'] = p['option_lp_main'] + p['option_lp_spouse']
        p['option_lp_wo_discount'] = (p['option_lp_wo_discount_and_gst_main'] + p['option_lp_wo_discount_and_gst_spouse']) * (1+self.with_gst)

#############
        p['option_dc_wo_discount_and_gst_main'] = dc * self.dc * coeff_discount
        p['discount_dc_wo_gst_main'] = p['option_dc_wo_discount_and_gst_main'] * self.promo_discount
        p['option_dc_main'] = p['option_dc_wo_discount_and_gst_main'] - p['discount_dc_wo_gst_main'] + round((p['option_dc_wo_discount_and_gst_main'] - p['discount_dc_wo_gst_main']) * self.with_gst, 2)

        p['option_dc_wo_discount_and_gst_spouse'] = dc * spouse * self.dc* coeff_discount
        p['discount_dc_wo_gst_spouse'] = p['option_dc_wo_discount_and_gst_spouse'] * self.promo_discount
        p['option_dc_spouse'] = p['option_dc_wo_discount_and_gst_spouse'] - p['discount_dc_wo_gst_spouse'] + round((p['option_dc_wo_discount_and_gst_spouse'] - p['discount_dc_wo_gst_spouse']) * self.with_gst, 2)

        p['option_dc'] = p['option_dc_main'] + p['option_dc_spouse']
        p['option_dc_wo_discount'] = (p['option_dc_wo_discount_and_gst_main'] + p['option_dc_wo_discount_and_gst_spouse']) * (1+self.with_gst)


#############

        p['total_premium_base'] = p['premium_main'] + p['premium_spouse'] + p['premium_kids']

        p['total_allowance'] = p['option_ttd'] + p['option_tpd'] + p['option_ahc']
        p['total_options'] = p['option_hsi'] + p['option_rssi'] + p['option_lp'] + p['option_dc']


        # p['total_premium'] =   p['total_options'] + p['total_allowance']+ p['total_premium_base']
        p['total_premium'] =   p['total_premium_base']

        p['total_options_wo_discount'] =  (p['option_dc_wo_discount'] +
                                           p['option_lp_wo_discount'] +
                                           p['option_rssi_wo_discount'] +
                                           p['option_hsi_wo_discount'])

        p['total_allowance_wo_discount'] = (p['option_ahc_wo_discount'] +
                                           p['option_tpd_wo_discount'] +
                                           p['option_ttd_wo_discount'])

        p['total_premium_wo_discount'] =   p['total_options_wo_discount'] + p['total_allowance_wo_discount']+ p['premium_wo_discount_main'] + p['premium_wo_discount_spouse'] + p['premium_wo_discount_kids']

        p['discount_percentage'] = self.promo_discount

#        p['option_ttd_spouse'] = spouse * self.ttd[ttd]* coeff_discount* self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
#        p['option_ttd_adults'] = round(p['option_ttd_main'] + p['option_ttd_spouse'],2)

#        p['option_tpd_main'] = self.tpd[tpd]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
#        p['option_tpd_spouse'] = spouse * self.tpd[tpd]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
#        p['option_tpd_adults'] = round(p['option_tpd_main'] + p['option_tpd_spouse'],2)




        # p['premium_spouse'] = round(spouse * si * self.coeff_si(si) * self.coeff_manual_occupation[manual_occupation_spouse] * coeff_discount * self.with_gst,2)
        # p['premium_kids'] = round(no_kids * min(si /2, 150000) * self.coeff_kid * coeff_discount * self.with_gst,2)
        #
        # p['premium_main'] = round(si * self.coeff_si(si) * self.coeff_manual_occupation[manual_occupation_main] * coeff_discount * self.with_gst ,2)
        # p['premium_spouse'] = round(spouse * si * self.coeff_si(si) * self.coeff_manual_occupation[manual_occupation_spouse] * coeff_discount * self.with_gst,2)
        # p['premium_kids'] = round(no_kids * min(si /2, 150000) * self.coeff_kid * coeff_discount * self.with_gst,2)
        #
        # p['option_ttd_main'] = self.ttd[ttd]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_ttd_spouse'] = spouse * self.ttd[ttd]* coeff_discount* self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_ttd_adults'] = round(p['option_ttd_main'] + p['option_ttd_spouse'],2)
        #
        # p['option_tpd_main'] = self.tpd[tpd]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_tpd_spouse'] = spouse * self.tpd[tpd]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_tpd_adults'] = round(p['option_tpd_main'] + p['option_tpd_spouse'],2)
        #
        # p['option_ahc_main'] = self.ahc[ahc]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_ahc_spouse'] = spouse * self.ahc[ahc]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_ahc_kids'] =  no_kids * self.ahc[ahc]* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['option_ahc_adults'] = round(p['option_ahc_main'] + p['option_ahc_spouse'] +  p['option_ahc_kids'],2)
        #
        # p['options_hsi_main'] = hsi * self.hsi['adult'] * coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_hsi_spouse'] = hsi * spouse * self.hsi['adult'] * coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_hsi_kids'] = hsi * no_kids * self.hsi['kid']* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_hsi_adults'] = round(p['options_hsi_main'] + p['options_hsi_spouse'] + p['options_hsi_kids'],2)
        #
        # p['options_rssi_main'] = rssi * self.rssi['adult']* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_rssi_spouse'] = rssi * spouse * self.rssi['adult']* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_rssi_kids'] = rssi * no_kids * self.rssi['kid']* coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_rssi_adults'] = round(p['options_rssi_main'] + p['options_rssi_spouse'] + p['options_rssi_kids'],2)
        #
        # p['options_lp_main'] = lp * self.lp * coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_lp_spouse'] = lp * spouse * self.lp * coeff_discount * self.coeff_manual_occupation[manual_occupation_main]  * self.with_gst
        # p['options_lp_adults'] = round(p['options_lp_main'] + p['options_lp_spouse'],2)
        #
        # p['options_dc_main'] = dc * self.dc * coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_dc_spouse'] = dc * spouse * self.dc * coeff_discount * self.coeff_manual_occupation[manual_occupation_main] * self.with_gst
        # p['options_dc_adults'] = round(p['options_dc_main'] + p['options_dc_spouse'],2)
        #
        #
        #
        # p['total_allowance'] = p['option_ahc_adults']+ p['option_tpd_adults'] + p['option_ttd_adults']
        # p['total_options'] = p['options_rssi_adults'] + p['options_lp_adults'] +  p['options_dc_adults']
        #
        #
        # p['total_option_ahc'] = p['option_ahc_main'] + p['option_ahc_spouse'] + p['option_ahc_kids']
        # p['total_premium_adults'] = p['premium_main'] + p['premium_spouse'] + p['premium_kids']
        # p['total_option_adults'] = p['option_ttd_main'] + p['option_ttd_spouse'] +\
        #                            p['option_tpd_main'] + p['option_tpd_spouse'] +\
        #                            p['option_ahc_main'] + p['option_ahc_spouse'] +\
        #                            p['options_rssi_main'] + p['options_rssi_spouse'] +\
        #                            p['options_lp_main'] + p['options_lp_spouse']+\
        #                            p['options_dc_main'] + p['options_dc_spouse']
        #
        # p['total_premium'] = p['premium_main'] + p['premium_spouse'] + p['premium_kids'] +\
        #                      p['option_ttd_main'] + p['option_ttd_spouse'] +\
        #                      p['option_tpd_main'] + p['option_tpd_spouse'] +\
        #                      p['option_ahc_main'] + p['option_ahc_spouse'] + p['option_ahc_kids'] +\
        #                      p['options_hsi_main'] + p['options_hsi_spouse'] + p['options_hsi_kids']+\
        #                      p['options_rssi_main'] + p['options_rssi_spouse'] + p['options_rssi_kids']+\
        #                      p['options_lp_main'] + p['options_lp_spouse']+\
        #                      p['options_dc_main'] + p['options_dc_spouse']
        #
        # p['discount_amount'] = p['total_premium'] * self.promo_discount / (1-self.promo_discount)
        # p['discount_percentage'] = self.promo_discount

        return (p)


def main():
   p1 = Tariff().premium(si = 100000, manual_occupation_main = False, spouse = True, no_kids = 1 , manual_occupation_spouse = False, ttd = 0, tpd = 0, ahc = 100, hsi = False, rssi = False, lp = False, dc = False)
   p2 = Tariff().premium(si = 200000, manual_occupation_main = True, spouse = True, no_kids = 2 , manual_occupation_spouse = True)
   p3 = Tariff().premium(si = 500000, manual_occupation_main = True, spouse = True, no_kids = 2 , manual_occupation_spouse = True)

   print p1['total_premium']

   print p1['option_ttd'], p1['option_tpd'], p1['option_ahc']



   # print p1['premium_main']
   # #print p2['premium_main']
   # #print p3['premium_main']
   #
   # print("==spouse==")
   # print p1['premium_spouse']
   # #print p2['premium_spouse']
   # #print p3['premium_spouse']
   #
   # print("==kid==")
   # print p1['premium_kid']
   # #print p2['premium_kid']
   # #print p3['premium_kid']
   #
   # print("==kids==")
   # print p1['premium_kids']
   # #print p2['premium_kids']
   # #print p3['premium_kids']
   #
   # print("==ttd==")
   # #print p1['option_ttd_wo_discount_and_gst_main']
   # #print p1['discount_ttd_wo_discount_and_gst_main']
   # #print p1['option_ttd_main']
   # print p1['option_ttd']
   #
   # print("==tpd==")
   # #print p1['option_tpd_wo_discount_and_gst_main']
   # #print p1['discount_tpd_wo_discount_and_gst_main']
   #
   # #print p1['option_tpd_main']
   # #print p1['option_tpd_spouse']
   # print p1['option_tpd']
   #
   # print("==ahc==")
   # #print p1['option_ahc_wo_discount_and_gst_main']
   # #print p1['discount_ahc_wo_discount_and_gst_main']
   # #print p1['option_ahc_main']
   # #print p1['option_ahc_spouse']
   # #print p1['option_ahc_kids']
   # print p1['option_ahc']
   #
   # print("===hsi===")
   # #print p1['option_hsi_main']
   # #print p1['option_hsi_spouse']
   # #print p1['option_hsi_kids']
   # print p1['option_hsi']
   #
   # print("===rssi===")
   # #print p1['option_rssi_main']
   # #print p1['option_rssi_spouse']
   # #print p1['option_rssi_kids']
   # print p1['option_rssi']
   #
   # print("==lp==")
   # #print p1['option_lp_main']
   # #print p1['option_lp_spouse']
   # print p1['option_lp']
   #
   # print("==dc==")
   # #print p1['option_dc_main']
   # #print p1['option_dc_spouse']
   # print p1['option_dc']
   #
   # print("==total==")
   # print p1['total_premium']
   # print p1['total_premium_wo_discount']
   # print p1['total_premium'] / p1['total_premium_wo_discount']
   #
   #
   # print p1['total_options']
   # print p1['total_options_wo_discount']
   # print p1['total_options'] / p1['total_options_wo_discount']

if __name__ == "__main__":
  main()