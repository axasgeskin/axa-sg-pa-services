__author__ = 'clement'


class Tariff() :

    prem = 15.00
    discount_table = {'single':0, 'spouse_or_kids': 0.05, 'family': 0.10 }

    def premium(self, spouse = 0, kids = 0 ) :


        if (spouse > 1) : spouse = 1
        if (spouse * kids > 0 ) :
            discount = self.discount_table['family']
        elif(spouse + kids > 0) :
            discount = self.discount_table['spouse_or_kids']
        else :
            discount = self.discount_table['single']
        return (round( (1+spouse + kids) * self.prem * (1- discount) , 2),discount)


    def premium_spouse_or_kid(self) :
        (prem_normal,none) = self.premium()
        (prem_global,none) = self.premium(spouse = 1)
        prem_spouse = prem_global - prem_normal
        return prem_spouse

#    def premium_family(self) :
#        return round(self.prem * (1- self.discount_table['family']),2)


def main():
   p = Tariff().premium(spouse = 1, kids = 1)
   print Tariff().premium(spouse = 1)
   print Tariff().premium()
   print Tariff().premium_spouse_or_kid()
#   print Tariff().premium_family()

if __name__ == "__main__":
  main()