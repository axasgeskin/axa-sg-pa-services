import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

from google.appengine.ext import db
from google.appengine.api import mail
from google.appengine.api import urlfetch

import datetime
import json

import tariff
import tariff_direct
import model

import logging
import email_contents


import random

package = 'AxaServices'


from sendgrid import SendGridClient
from sendgrid import Mail


class PremiumIndividual(messages.Message):
    premium_self = messages.StringField(1)
    premium_spouse_or_kid = messages.StringField(2)


class Premium(messages.Message):
    premium = messages.StringField(1)
    discount = messages.StringField(2)

class Criteria(messages.Message):
    spouse = messages.IntegerField(1)
    kids = messages.IntegerField(2)
    customer_encrypted_data = messages.StringField(3)
    email = messages.StringField(4)
    policy_data = messages.StringField(5)





class Payment(messages.Message):
    url = messages.StringField(1)
    amt = messages.StringField(2)
    ref = messages.StringField(3)

class CheckPayment(messages.Message):
     status = messages.StringField(1)
     message = messages.StringField(2)
     premium = messages.StringField(3)
     previous_id = messages.StringField(4)


class DirectCriteria(messages.Message):
    si = messages.IntegerField(1)
    manual_occupation_main = messages.BooleanField(2)
    spouse = messages.IntegerField(3)
    manual_occupation_spouse = messages.BooleanField(4)
    no_kids  = messages.IntegerField(5)
    customer_encrypted_data = messages.StringField(6)
    email = messages.StringField(7)
    policy_data = messages.StringField(8)
    ttd = messages.IntegerField(9)
    tpd = messages.IntegerField(10)
    ahc = messages.IntegerField(11)
    hsi = messages.BooleanField(12)
    rssi = messages.BooleanField(13)
    lp = messages.BooleanField(14)
    dc = messages.BooleanField(15)
    referral = messages.StringField(16)


class DirectPremium(messages.Message):
    total_premium = messages.StringField(1)
    total_premium_adults = messages.StringField(2)
    total_premium_discount = messages.StringField(3)
    total_premium_wo_discount = messages.StringField(5)
    discount_percentage =  messages.StringField(6)
    option_ttd_adults= messages.StringField(7)
    option_tpd_adults= messages.StringField(8)
    total_option_ahc= messages.StringField(9)
    options_hsi_adults= messages.StringField(10)
    options_rssi_adults= messages.StringField(11)
    options_lp_adults= messages.StringField(12)
    options_dc_adults= messages.StringField(13)
    option_ahc_adults = messages.StringField(14)
    total_allowance =  messages.StringField(15)
    total_options =  messages.StringField(16)


class Contract(messages.Message):
    transidmerchant = messages.StringField(1)
    totalamount = messages.FloatField(2)
    status = messages.StringField(3)
    paytype = messages.StringField(4)
    approvalcode = messages.StringField(5)
    bankresponsecode = messages.StringField(6)
    trntype = messages.StringField(7)
    created = messages.StringField(8)
    recurrentid = messages.StringField(9)
    data = messages.StringField(10)
    error = messages.StringField(11)
    errormsg = messages.StringField(12)
    env =  messages.StringField(13)
    mida = messages.StringField(14)

class ContractCollection(messages.Message):
    contracts = messages.MessageField(Contract, 1, repeated=True)



@endpoints.api(name='axapaapi', version='v1')
class AxaPaApi(remote.Service):
    """premiums API v1."""

    @endpoints.method(message_types.VoidMessage, PremiumIndividual,
                      path='premium/list', http_method='GET',
                      name='premium.list')
    def premium_list(self, request):
        (ps,none) = tariff.Tariff().premium()
        return  PremiumIndividual(premium_self="{:.2f}".format(ps),
                                  premium_spouse_or_kid= "{:.2f}".format(tariff.Tariff().premium_spouse_or_kid()))


    CRITERIA_RESOURCE = endpoints.ResourceContainer(
            Criteria
            )
    @endpoints.method(CRITERIA_RESOURCE, Premium,
                      path='premium/compute', http_method='POST',
                      name='premium.compute')
    def premium_compute(self, request):
        (premium,discount) =tariff.Tariff().premium(spouse = request.spouse, kids=request.kids)
        return  Premium(premium="{:.2f}".format(premium), discount="{:.2f}".format(discount) )



    CRITERIA_RESOURCE = endpoints.ResourceContainer(
            Criteria
            )
    @endpoints.method(CRITERIA_RESOURCE, Payment,
                      path='hsbc/payment/url', http_method='POST',
                      name='hsbc.payment.url')
    def payment_url(self, request):

        t = tariff.Tariff()
        (ps, none) = t.premium(spouse = request.spouse, kids=request.kids)

        data = {
            'spouse' : request.spouse,
            'kids' : request.kids,
            'customer_encrypted_data' : request.customer_encrypted_data,
            'email' : request.email,
            'policy_data' : request.policy_data
        }

        amt = "{:.2f}".format(ps)



        mid = { 'UAT' :{
                    'all':{'mida' : '20150122003', 'midb' : '20150122004'} ,
                    'url_payment': "https://test.wirecard.com.sg/easypay2/paymentpage.do",
                    'return_url' : "https://150619-dot-axa-sg-pa-services.appspot.com/return/",
                    'statut_url' : "https://axa-sg-pa-services.appspot.com/_ah/api/axapaapi/v1/paymentstatus/"
                    },
                'LIVE' :{
                    'all':{'mida' : '101201435694', 'midb' : ''} ,
                    'url_payment': "https://securepayments.telemoneyworld.com/easypay2/paymentpage.do",
                    'return_url' : "https://axa-sg-pa-services.appspot.com/return/",
                    'statut_url' : "https://axa-sg-pa-services.appspot.com/_ah/api/axapaapi/v1/paymentstatus"
                    }
            }

        env = 'LIVE'
        product = 'HSBCPA'

        ref = product + "%06d"%(random.randint(0,100000))
        i =0
        while (model.Wirecard.get_by_key_name(ref) and i <100):
            ref = product + "%06d"%(random.randint(0,100000))
            i = i + 1
            logging.warning("== REF COLLISION =" + str(i))


        url_payment = mid[env]['url_payment']
        mida = mid[env]['all']['mida']
        midb = mid[env]['all']['midb']

        return_url = mid[env]['return_url'] + ref
        statut_url = mid[env]['statut_url']
        recurring_string = ""

        wc = model.Wirecard(key_name=ref,transidmerchant = ref,totalamount = float(amt), data = json.dumps(data), env = env, mida = mida, product = product)
        wc.put()

        url = "%s?mid=%s&ref=%s&amt=%s&cur=SGD&returnurl=%s&statusurl=%s%s&userfield1=%s"\
              %(url_payment,mida,ref, amt, return_url,statut_url,recurring_string,  product)

        logging.info(url)

        return  Payment(url=url, amt=amt, ref=ref)



    PAYMENTSTATUS_RESOURCE = endpoints.ResourceContainer(
        TM_RefNo = messages.StringField(1),
        TM_DebitAmt = messages.StringField(2),
        TM_PaymentType = messages.StringField(3),
        TM_Status = messages.StringField(4),
        TM_Error = messages.StringField(5),
        TM_ErrorMsg = messages.StringField(6),
        TM_ApprovalCode = messages.StringField(7),
        TM_BankRespCode = messages.StringField(8),
        TM_TrnType = messages.StringField(9),
        TM_RecurrentId = messages.StringField(10),
        TM_UserField1 = messages.StringField(11)
            )

    @endpoints.method(PAYMENTSTATUS_RESOURCE ,message_types.VoidMessage,
                      path='paymentstatus', http_method='POST',
                      name='payment.status')
    def payment_status(self, request):

        wc = model.Wirecard.get_by_key_name(request.TM_RefNo)
        if not wc:
            # not found error
            logging.warning("No record found: %s" % request.TM_RefNo)
        elif request.TM_Error:
            # payment gateway error
            logging.warning("ERROR: %s" % request.TM_Error)
            wc.error = request.TM_Error
            wc.errormsg = request.TM_ErrorMsg
            wc.put()
        else:
            if float(wc.totalamount) != float(request.TM_DebitAmt):
                # amount mismatch
                wc.error = 'AMOUNT_MISMATCH'
                wc.errormsg = "Amount Mismatch for %s: %s - %s" % (request.TM_RefNo, wc.totalamount, request.TM_DebitAmt)
                wc.put()

            else:
                wc.status = request.TM_Status
                wc.paytype = request.TM_PaymentType
                wc.approvalcode = request.TM_ApprovalCode
                wc.bankresponsecode = request.TM_BankRespCode
                wc.trntype = request.TM_TrnType
                wc.recurrentid = request.TM_RecurrentId
                wc.put()

                try :
                    Email().sent_sg(request.TM_RefNo)

                except Exception as e:
                    logging.error('error to send email = %s'%(request.TM_RefNo))
                    logging.warning(e)

                try :
                        url_xiti = 'https://logws1344.ati-host.net/hit.xiti?s=559622&s2=6&p=personal_accident::step_8::confirmation_page|hit_server_side&x1=[personal_accident]&x2=[step_8]&x3=[confirmation_page|hit_server_side]&tp=conf1&idcart=%s&cmd=%s&st=3'%(request.TM_RefNo, request.TM_RefNo)
                        result = urlfetch.fetch(url_xiti)
                        logging.info('url_xiti='+url_xiti )
                        logging.info(result.status_code)

                except Exception as e:
                        logging.error('error xiti = %s'%(request.TM_RefNo))
                        logging.warning(e)




        return  message_types.VoidMessage()



    CHECKPAYMENT_RESOURCE = endpoints.ResourceContainer(
        id = messages.StringField(1)
            )

    @endpoints.method(CHECKPAYMENT_RESOURCE ,message_types.VoidMessage,
                      path='sendemail/{id}', http_method='GET',
                      name='payment.sendemail')

    def sendemailsg(self, request):
        Email().sent_sg(request.id)
        return  message_types.VoidMessage()





    @endpoints.method(CHECKPAYMENT_RESOURCE ,CheckPayment,
                          path='paymentcheck/{id}', http_method='GET',
                          name='payment.check')

    def payment_check(self, request):

            wc = model.Wirecard.get_by_key_name(request.id)


            if not wc:
                    # not found error
                    raise endpoints.InternalServerErrorException("no policy found, please contact assistance")
            else:
                if(wc.status ==  "YES" and wc.error != 'REFERRAL') :
                    cp =  CheckPayment(status = 'OK', message = 'Thank you for your payment' , premium = None, previous_id = None )
                elif( wc.error == 'REFERRAL') :
                        cp =  CheckPayment(status = 'OK', message = 'REFERRAL' , premium = None, previous_id = None )
                else :
                        cp =  CheckPayment(status = 'ERROR', message = 'Sorry, an error occurred during the payment' , premium = None, previous_id = None )
            return  cp


    DIRECT_CRITERIA_RESOURCE = endpoints.ResourceContainer(
            DirectCriteria
            )
    @endpoints.method(DIRECT_CRITERIA_RESOURCE, DirectPremium,
                      path='direct/premium/compute', http_method='POST',
                      name='direct/premium.compute')

    def direct_premium_compute(self, request):
        p = tariff_direct.Tariff().premium(si = request.si,
                                           manual_occupation_main = request.manual_occupation_main,
                                           spouse = request.spouse,
                                           manual_occupation_spouse = request.manual_occupation_spouse,
                                           no_kids=request.no_kids,
                                           ttd = request.ttd,
                                           tpd = request.tpd,
                                           ahc = request.ahc,
                                           hsi = request.hsi,
                                           rssi = request.rssi,
                                           lp = request.lp,
                                           dc = request.dc)

        p_wo_discount_t = tariff_direct.Tariff()
        p_wo_discount_t.promo_discount = 0
        p_wo_discount = p_wo_discount_t.premium(si = request.si,
                                           manual_occupation_main = request.manual_occupation_main,
                                           spouse = request.spouse,
                                           manual_occupation_spouse = request.manual_occupation_spouse,
                                           no_kids=request.no_kids,
                                           ttd = request.ttd,
                                           tpd = request.tpd,
                                           ahc = request.ahc,
                                           hsi = request.hsi,
                                           rssi = request.rssi,
                                           lp = request.lp,
                                           dc = request.dc)




        return DirectPremium(
            total_premium = "{:.2f}".format(p['total_premium']),
            total_premium_wo_discount = "{:.2f}".format(p['total_premium_wo_discount'] ),
            total_premium_discount = "{:.2f}".format(p['total_premium_wo_discount'] - p['total_premium']),
            discount_percentage = "{:.2f}".format(p['discount_percentage']),

            total_premium_adults = "{:.2f}".format(p['total_premium_base']),
            total_allowance = "{:.2f}".format(p['total_allowance']),
            total_options = "{:.2f}".format(p['total_options']),

            option_ttd_adults = "{:.2f}".format(p['option_ttd']),
            option_tpd_adults= "{:.2f}".format(p['option_tpd']),
            total_option_ahc= "{:.2f}".format(p['option_ahc']),
            option_ahc_adults= "{:.2f}".format(p['option_ahc']),
            options_hsi_adults= "{:.2f}".format(p['option_hsi']),
            options_rssi_adults= "{:.2f}".format(p['option_rssi']),
            options_lp_adults= "{:.2f}".format(p['option_lp']),
            options_dc_adults= "{:.2f}".format(p['option_dc'])

        )



    @endpoints.method(DIRECT_CRITERIA_RESOURCE, Payment,
                      path='direct/payment/url', http_method='POST',
                      name='direct.payment.url')
    def direct_payment_url(self, request):

        env = 'LIVE'
        product = "PADIRECT"


        p = tariff_direct.Tariff().premium(si = request.si,
                                           manual_occupation_main = request.manual_occupation_main,
                                           spouse = request.spouse,
                                           manual_occupation_spouse = request.manual_occupation_spouse,
                                           no_kids=request.no_kids,
                                           ttd = request.ttd,
                                           tpd = request.tpd,
                                           ahc = request.ahc,
                                           hsi = request.hsi,
                                           rssi = request.rssi,
                                           lp = request.lp,
                                           dc = request.dc)


        mid = { 'UAT' :{
                    'all':{'mida' : '20150122003', 'midb' : '20150122004'} ,
                    'url_payment': "https://test.wirecard.com.sg/easypay2/paymentpage.do",
                    'return_url' : "https://150619-dot-axa-sg-pa-services.appspot.com/return_direct/",
                    'statut_url' : "https://150619-dot-axa-sg-pa-services.appspot.com/_ah/api/axapaapi/v1/paymentstatus"
                    },
                'LIVE' :{
                    'all':{'mida' : '104201537224', 'midb' : ''} ,
                    'url_payment': "https://securepayments.telemoneyworld.com/easypay2/paymentpage.do",
                    'return_url' : "https://axa-sg-pa-services.appspot.com/return_direct/",
                    'statut_url' : "https://axa-sg-pa-services.appspot.com/_ah/api/axapaapi/v1/paymentstatus"
                    }

            }


        ref = product + "%06d"%(random.randint(0,100000))
        i =0
        while (model.Wirecard.get_by_key_name(ref) and i <100):
            ref = product + "%06d"%(random.randint(0,100000))
            i = i + 1
            logging.warning("== REF COLLISION =" + str(i))

        url_payment = mid[env]['url_payment']
        mida = mid[env]['all']['mida']
        midb = mid[env]['all']['midb']

        return_url = mid[env]['return_url'] + ref
        statut_url = mid[env]['statut_url']
        recurring_string = ""

        amt = "{:.2f}".format(p['total_premium'])

        data = {
            'si' : request.si,
            'manual_occupation_main' : request.manual_occupation_main,
            'spouse'  : request.spouse,
            'manual_occupation_spouse' : request.manual_occupation_spouse,
            'no_kids' :request.no_kids,
            'ttd' : request.ttd,
            'tpd' : request.tpd,
            'ahc' : request.ahc,
            'hsi' : request.hsi,
            'rssi' :request.rssi,
            'lp' : request.lp,
            'dc' : request.dc,
            'customer_encrypted_data' : request.customer_encrypted_data,
            'email' : request.email,
            'policy_data' : request.policy_data,
            'premium_details' : json.dumps(p)
        }


        error = None
        errormsg = None
        if(request.si >= 1000000 or request.referral) :
            error = 'REFERRAL'
            errormsg = request.referral

        wc = model.Wirecard(key_name=ref,transidmerchant = ref,totalamount = float(amt), data = json.dumps(data), env = env, mida = mida, product=product, error = error, errormsg = errormsg)
        wc.put()

        logging.info("==ref==")
        logging.info(request.referral)
        logging.info(error)

        if(error == None) :
            url = "%s?mid=%s&ref=%s&amt=%s&cur=SGD&returnurl=%s&statusurl=%s%s&userfield1=%s"\
                  %(url_payment,mida,ref, amt, return_url,statut_url, recurring_string,  product)

            logging.info(url)

            try :
                        url_xiti = 'https://logws1344.ati-host.net/hit.xiti?s=559622&s2=6&p='  +\
                                   'personal_accident::step_7::pay_now&x1=[personal_accident]&x2=[step_7]&x3=[pay_now]&tp=pre1'+\
                                   '&idcart=' + ref + '&orderid=' + ref + '&roimt=' + amt + '&totalATI=' + amt+\
                                   '&totalTF=0' + '&discountATI=0' + '&discountTF=0' + '&newcus=1'+ '&shipATI=0'+ '&shipTF=0'+ '&tax=0' + '&paym=11'+ '&status=1' + '&delivery=1'

                        result = urlfetch.fetch(url_xiti)
                        logging.info('url_xiti preorder='+url_xiti )
                        logging.info(result.status_code)

            except Exception as e:
                        logging.error('error xiti = %s'%(ref))
                        logging.warning(e)






        else:
            url = return_url


        logging.info(url)
        return  Payment(url=url, amt=amt, ref=ref )






    DATES_RESOURCE = endpoints.ResourceContainer(
        product = messages.StringField(1),
        date_start = messages.StringField(2),
        date_stop = messages.StringField(3),
        token = messages.StringField(4)
        )



    @endpoints.method(DATES_RESOURCE ,ContractCollection,
                      path='data/{token}/{product}/{date_start}/{date_stop}', http_method='GET',
                      name='data.list')

    def policies_list(self, request):

        if(request.token != 'd984abdd33b3d8e0b862d5844786cea9a6ecbb502d0f5731d8b897c2ef9c53d3') :
                raise endpoints.InternalServerErrorException("invalid token")


        date_start = datetime.datetime.strptime(request.date_start, '%Y-%m-%d') + datetime.timedelta(days=-1)
        date_stop  = datetime.datetime.strptime(request.date_stop, '%Y-%m-%d')
        sql = "SELECT * FROM Wirecard WHERE product='%s' AND created >= DATETIME('%s') AND created < DATETIME('%s') order by created"% (request.product, date_start.strftime('%Y-%m-%d 16:00:00'), date_stop.strftime('%Y-%m-%d 15:59:59'))

        logging.info("Running Daily SQL: %s" % sql )
        q1 = db.GqlQuery(sql)

        policies =[]

        for wc in q1:
            if (wc.status == "YES" or wc.error != None):
                policy = Contract(
                        transidmerchant = wc.transidmerchant,
                        totalamount = wc.totalamount,
                        status = wc.status,
                        paytype = wc.paytype,
                        approvalcode = wc.approvalcode,
                        bankresponsecode = wc.bankresponsecode,
                        trntype = wc.trntype,
                        created = str(wc.created + datetime.timedelta(hours=8)),
                        recurrentid = wc.recurrentid,
                        data = wc.data,
                        error = wc.error,
                        errormsg = wc.errormsg,
                        env = wc.env,
                        mida = wc.mida
                    )
                policies.append(policy)

        return  ContractCollection(contracts=policies)





class Email :


        def sent_sg(self, id):

            wc = model.Wirecard.get_by_key_name(id)
            if not wc:
                    # not found error
                    raise endpoints.InternalServerErrorException("no policy found, please contact assistance")
            else:
                if(wc.status ==  "YES") :

                    policy = json.loads(wc.data)


                    data_dict = {}
                    data_dict['reference'] = wc.transidmerchant
                    data_dict['premium'] = wc.totalamount
                    message = Mail()

                    logging.info(wc.product)

                    if(wc.product == 'HSBCPA') :

                        policy['policy_data'] = json.loads(policy['policy_data'])

                        data_dict['spouse'] = 'None'
                        if(policy['spouse'] > 0) :
                                    data_dict['spouse'] = 'Included'
                        data_dict['children'] = 'None'
                        if(policy['kids'] > 0) :
                                    data_dict['children'] = str(policy['kids'])
                        data_dict['start_date'] =  policy['policy_data']['start_date'][8:10] + "/" + policy['policy_data']['start_date'][5:7] + "/" + policy['policy_data']['start_date'][0:4]
                        data_dict['end_date'] = policy['policy_data']['end_date']


                        logging.info(data_dict)

                        message.set_subject("Confirmation of your insurance purchase / %s"%(data_dict['reference']))
                        message.set_html(email_contents.client_text_html % data_dict)
                        message.set_text(email_contents.client_text % data_dict )
                        message.set_from('Axa Insurance Singapore<banca_hsbc@axa.com.sg>')
                        message.add_attachment('logo.png','logo.png')
                        message.add_content_id('logo.png','id-logo')
                        message.add_attachment('HSBC_PA_PolicyWording.pdf','HSBC_PA_PolicyWording.pdf')
                        message.add_to(policy['email'])
                        message.add_bcc('CC <cchaumette@getvalue.fr>')

                    else :
                        message.set_subject("Confirmation of your insurance purchase / %s"%(data_dict['reference']))
                        message.set_html(email_contents.client_text_html2 % data_dict)
                        message.set_text(email_contents.client_text2 % data_dict )
                        message.set_from('Axa Insurance Singapore<preferred@axa.com.sg>')
                        message.add_attachment('logo.png','logo.png')
                        message.add_content_id('logo.png','id-logo')
                        message.add_attachment('SmartCover_PolicyWording.pdf','SmartCover_PolicyWording.pdf')
                        message.add_to(policy['email'])
                        message.add_bcc('CC <cchaumette@getvalue.fr>')



                    # use the Web API to send your message
                    logging.warning('sendgrid')
                    sg = SendGridClient('getvalue', 'sg129@xa', secure=True)
                    r = sg.send(message)
                    logging.warning(r)





                    #
					 #  else :
						# message_c_body = email_contents.client_text2 % data_dict
						# message_c_html = email_contents.client_text_html2 % data_dict
                    #message.add_content_id('AXA Singapore _ Smart PA.pdf','id-pdf')


                    #
                    # else :
                    #     message.set_from('Axa Insurance Singapore<preferred@axa.com.sg>')
                    #

















APPLICATION = endpoints.api_server([AxaPaApi])