#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2

class MainHandler(webapp2.RequestHandler):
    def get(self,id):
        self.redirect("https://axa-sg-pa.appspot.com/hsbc_thank_you/#/" + id)

class MainHandler2(webapp2.RequestHandler):
    def get(self,id):
        self.redirect("https://150619-dot-axa-sg-pa.appspot.com/direct_thank_you/#/" + id)


app = webapp2.WSGIApplication([
    ('/return/(.*)', MainHandler),
    ('/return_direct/(.*)', MainHandler2)
], debug=True)
